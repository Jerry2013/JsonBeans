package jerry.jsonbeans.models;

/**
 * @author Jerry Ou
 * @version 1.0 2016-05-03 14:36
 * @since JDK 1.6
 */
public class FieldDescribe {

    public String name;
    public String type;
    public String importype;

}
